<?php

/**
 * This is the model class for table "classes".
 *
 * The followings are the available columns in table 'classes':
 * @property integer $id
 * @property string $name
 * @property integer $year
 * @property string $class
 * @property string $subject
 * @property string $date_added
 * @property integer $lesson_id
 * @property string $content
 *
 * The followings are the available model relations:
 * @property Lessons $lesson
 */
class Classes extends CActiveRecord
{
	protected function beforeSave ()
	{
		$this->date_added = Formatter::dateForSql($this->date_added);
		return parent::beforeSave();
	}

	public function class_r()
	{
		return str_replace(',', '', $this->class);
	}

	public function lessonLink()
	{
		return $this->lesson_id == 0 ? 'Not Set' : sprintf('<a href="%s/lessons/%s">%s</a>', Yii::app()->baseUrl, $this->lesson_id, $this->lesson_id);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'classes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, year, class, subject, date_added, lesson_id, content', 'required'),
			array('year, lesson_id', 'numerical', 'integerOnly'=>true),
			array('name, subject', 'length', 'max'=>100),
			array('class', 'length', 'max'=>32),
			array('content', 'length', 'max'=>8192),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, year, class, subject, date_added, lesson_id, content', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lesson' => array(self::BELONGS_TO, 'Lessons', 'lesson_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'year' => 'Academic Year Start',
			'class' => 'Class',
			'subject' => 'Subject',
			'date_added' => 'Date Scheduled',
			'lesson_id' => 'Lesson',
			'content' => 'Content',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('year',$this->year);
		$criteria->compare('class',$this->class,true);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('date_added',$this->date_added,true);
		$criteria->compare('lesson_id',$this->lesson_id);
		$criteria->compare('content',$this->content,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Classes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
