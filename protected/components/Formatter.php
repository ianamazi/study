<?php
class Formatter
{
	public static function dateForSql($value = null, $format = 'd-M-yyyy')
	{
		if ($value == null || $value == '') return '';
		if (CDateTimeParser::parse($value,'yyyy-M-d')) return $value; // leave in original format

		$ts = CDateTimeParser::parse($value,$format,array('month'=>1,'day'=>1,'hour'=>0,'minute'=>0,'second'=>0));
		if($ts !== false)
		{
			$dt = CTimestamp::formatDate('Y-m-d H:i:s', $ts);
			return $dt;
		}

		throw new CHttpException(100, sprintf('Could not parse date %s. Expected format %s', $value, $format));
	}

	public static function date($date, $time = false, $compact = 0)
	{
		if ($date == null || $date == '0000-00-00') return "Not Mentioned";
		$dt = is_string($date) ? strtotime($date) : $date;
		if ($compact) return sprintf('<span class="date compact" title="%s">%s</span>', date('d M Y', $dt), date('D h:i', $dt));
		$time = $time ? ' h:i a' : '';
		return date('D, d M Y', $dt) . $time;
	}

	public static function includeBlank($list, $text = 'Not Set')
	{
		//array_merge was messing up the keys using the index
		$data = array('' => $text);
		foreach ($list as $key=>$value) $data[$key] = $value;
		return $data;
	}

	public static function errors($m, $glue = ', ')
	{
		// based on CHtml::errorSummary
		$content = array();
		foreach($m->getErrors() as $errors)
		{
			foreach($errors as $error)
			{
				if($error!='')
					$content[]= $error;
			}
		}
		return implode($glue, $content);
	}

	public static function arrayGroupBy($items, $key, $column)
	{
		return self::groupArray($items, $key, $column, '');
	}

	private static function groupArray(&$items, $key, $column, $group)
	{
		$op = array();
		foreach($items as $itm)
		{
			if ($itm[$column] == $group)
			{
				$op[] = array('obj' => $itm, 'items' => self::groupArray($items, $key, $column, $itm[$key]));
			}
		}
		return $op;
	}

	public static function htmlEditor($form, $model,$field)
	{
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile('//tinymce.cachefly.net/4.1/tinymce.min.js');
		$cs->registerScript('tinymce-init', "tinymce.init({
	selector:'textarea.html-editor',
	height: 250,
	plugins: 'link,image'
});");
		
		echo $form->textArea($model, $field, array('cols' => 50, 'class' => 'html-editor'));
	}

	public static function datePicker($controller, $model, $field)
	{
		$baseUrl = Yii::app()->baseUrl;
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($baseUrl.'/js/jui-datepicker.js');

		$allowUptoNow = array_search($field, array('from', 'to', 'on')) !== false;
		$name = get_class($model);
		$controller->widget('zii.widgets.jui.CJuiDatePicker',array(
			'name'=> sprintf('%s[%s]', $name, $field),
			'id'=>sprintf('%s_%s', $name, $field),
			'value'=>Yii::app()->dateFormatter->format("d-M-y",strtotime($model->$field)),
				'options'=>array(
					'showAnim'=>'fold',
					'changeYear' => true,
					'changeMonth' => true,
					'yearRange' => '+0:+1',
					'dateFormat' => 'dd-mm-yy',
				),
				'htmlOptions'=>array('style'=>'height:20px;'),
		));
		echo ' <a href="#" class="clear-date">clear</a>';
	}

	public static function Singular($text)
	{
		include_once 'ci_inflector.php';
		return Inflector::singular($text);
	}
}
?>