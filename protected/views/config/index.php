<?php
/* @var $this ConfigController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Configs',
);

$this->menu=array(
	array('label'=>'Create Config', 'url'=>array('create')),
	array('label'=>'Manage Config', 'url'=>array('admin')),
);
?>
<script type="text/javascript">
function loadWithType()
{
	location = "./?type=" + $("#type").val();
}
</script>
<h1>Configs</h1>
<?php echo CHtml::dropDownList('type', $type, Formatter::includeBlank(Config::$types)); ?>
<?php echo ' ' . CHtml::button('GO', array('onclick'=>'js:loadWithType()' )); ?>
<br /><br />
<table>
<?php
$this->renderPartial('_view');
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
</table>
