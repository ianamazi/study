<?php
/* @var $this ClassesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Classes',
);

if (!UserIdentity::user_is('student'))
$this->menu=array(
	array('label'=>'Create Class', 'url'=>array('create')),
	array('label'=>'Manage Classes', 'url'=>array('admin')),
);
?>

<h1>Classes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
