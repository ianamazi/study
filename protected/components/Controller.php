<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	function getAccessRules($returnList = 0)
	{
		$name = $this->getId();
		
		$rules = array(
			'config' => 'admin',
			'users' => 'admin',
			'lessons' => array('teacher', 'index' => '*', 'view' => '*', 'subject' => '*'),
			'classes' => array('teacher', 'index' => '*', 'view' => '*', 'subject' => '*'),
		);
		
		if (!isset($rules[$name])) throw new Exception(sprintf("Access Rules not set for %s controller", $name));

		$action = $this->getAction()->getId();
		$what = $rules[$name];

		if ($action == 'me' || $action == 'updateme') // TODO: UpdateMe url
		{
			$me = array('locations' => 'location', 'users' => '*');
			if (!isset($me[$name])) throw new Exception(sprintf("%s not valid on controller %s", $action, $name));
			$who = $me[$name];
			$can = $who == '*' || 1 || $this->user_is($who); // TODO: All users belong to location for now
		}
		else if (is_array($what))
		{
			$dev = UserIdentity::user_is('dev');
			if ($returnList)
			{
				$allowed = array();
				foreach ($what as $name=>$arr)
				{
					if (array_search($type, $arr) !== false) $allowed[] = $name;
					else if ($dev) $allowed[] = $name;
				}
				return $allowed;
			}
			
			if (isset($what[0]) && UserIdentity::user_is($what[0]))
				$can = true;
			else if (!isset($what[$action])) throw new Exception(sprintf("Access Rules not set for action %s of %s controller", $action, $name));
			$can = $dev || $what[$action] == '*' || UserIdentity::user_is($what[$action]);
		}
		else
		{
			if ($returnList) return '*';
			$can = UserIdentity::user_is($what);
			if ($name == 'users' && $action == 'signup' && $can == false) $can = 1;
		}

		return array(array( $can ? 'allow' : 'deny', 'users'=>array('*')));
	}
}