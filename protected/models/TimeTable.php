<?php
class TimeTable
{
	static function render()
	{
		if (UserIdentity::user_is('notloggedin')) return;

		$tt = self::getData();
		echo '<table border="1" class="tbl">';
		echo ' <tr><th>Day</th>';
		foreach ($tt as $class)
			echo '  <th title="' . sprintf('From: %s, To: %s', $class['from'], $class['to']) . '">' . $class['name'] . '</th>' . PHP_EOL;
		echo ' </tr>';
		
		$classes = self::getClasses();

		$days = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri');
		foreach ($days as $d=>$n)
		{
			echo ' <tr>';
			$day = new DateTime('monday this week'); $day = $day->add(new DateInterval('P'.$d.'D'));
			echo '<th>' . $n . ' (' . $day->format('d.m') . ')</th>';
			foreach ($tt as $class)
			{
				echo '  <td>' . 'None' . '</td>' . PHP_EOL;
			}
			echo ' </tr>';
		}

		echo ' </tr>';
		echo '</table>';
	}
	
	static function getData($for = 'display')
	{
		$tt = Config::byType('timetable');
		$op = array();
		foreach ($tt as $class)
		{
			if ($for == 'combo')
			{
				if ($class->data2 == 'break') continue;
				$op[$class->name] = sprintf('%s (%s)', $class->name, $class->data1);
			}
			else
			{
				$time = explode(' to ', $class->data1);
				$op[] = array('name' => $class->name, 'from' => $time[0], 'to' => $time[1]);
			}
		}
		return $op;
	}

	static function getClasses()
	{
		$cri = array();
	}

}
?>