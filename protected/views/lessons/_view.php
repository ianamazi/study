<?php
/* @var $this LessonsController */
/* @var $data Lesson */
?>

<?php
	if (UserIdentity::user_is('teacher'))
		echo CHtml::link('...', array('update', 'id'=>$data->id), array('class'=>'right'));
	$what = $data->class_r() . ' / ' . $data->subject . ' / Chapter: ' . $data->chapter . ' / ' . $data->name;
	echo CHtml::link($what, array('view', 'id'=>$data->id)); ?>
