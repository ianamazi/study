<?php

/**
 * This is the model class for table "config".
 *
 * The followings are the available columns in table 'config':
 * @property integer $id
 * @property string $type
 * @property string $name
 * @property string $data1
 * @property string $data2
 */
class Config extends CActiveRecord
{
	public static $types = array('class' => 'Class', 'subject' => 'Subject', 'teacher' => 'Teacher', 'timetable' => 'Time Table', 'classtimetable' => 'Class Time Table');

	public static function byType($type)
	{
		$criteria=new CDbCriteria;
		$criteria->compare('type',$type,true);
		$dp = new CActiveDataProvider('Config', array(
			'criteria'=>$criteria,
		));
		return $dp->getData();
	}

	public static function bulkCreate($type, $data = false)
	{
		//$type = '7ATimeTable';
		$existing = self::byType($type);
		$rows = array();
		foreach ($existing as $cfg) $rows[$cfg->name] = $cfg;
		$existing = $rows;
		if (!$data)
		{
			$headers = array(
				'teacher' => 'Subjects	Classes',
				'subject' => 'Class	Empty',
				'timetable' => 'Timing	Config'
			);
			$rows = array('#Name	' . (isset($headers[$type]) ? $headers[$type] : 'Data1	Data2'));
			foreach ($existing as $cfg)
				$rows[] = $cfg->name . '	' . $cfg->data1 . '	' . $cfg->data2;
			return implode(PHP_EOL, $rows);
		}

		$msg = array();
		$rows = explode(PHP_EOL, $data);
		$line = 1;
		foreach ($rows as $row)
		{
			if ($row == '' || $row[0] ==  '#') continue;
			$bits = explode('	', $row);
			if (count($bits) < 3)
			{
				$msg[] = '#' . $line . ' - ' . $bits[0] . ' needs 3 fields';
			}
			else
			{
				$name = $bits[0];
				$new = !isset($existing[$name]);
				$model = $new ? new Config : $existing[$name];
				$model->name = $name;
				$model->type = $type;
				$model->data1 = $bits[1];
				$model->data2 = $bits[2];
				if ($model->save())
				{
					$msg[] = '#' . $line . ' - ' . $name . ($new ? ' (new)' : '') . ' saved ok';
				}
				else
				{
					$msg[] = '#' . $line . ' - ' . $name . ' error - ' . Formatter::errors($model);
				}
			}
			$line++;
		}
		return implode('<br />' . PHP_EOL, $msg);
	}

	private $classVal;

	public function getClass()
	{
		if ($this->classVal != null) return $this->classVal;
		if (substr($this->type, -9) == 'TimeTable');
			return substr($this->type, 0, strlen($this->type) - 9);
		return null;
	}

	public function setClass($value)
	{
		$this->classVal = $value;
	}

	public function getTypeEx()
	{
		return substr($this->type, -9) == 'TimeTable' ? 'classtimetable' : $this->type;
	}

	public function setTypeEx($value)
	{
		$this->type = $value;
	}

	protected function beforeSave()
	{
		if ($this->type == 'classtimetable')
			$this->type = $this->classVal . 'TimeTable';
		return parent::beforeSave();
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'config';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type, name, data1', 'required'),
			array('type', 'length', 'max'=>32),
			array('name', 'length', 'max'=>128),
			array('data1, data2', 'length', 'max'=>2048),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, type, name, data1, data2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type' => 'Type',
			'name' => 'Name',
			'data1' => 'Data1',
			'data2' => 'Data2',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('data1',$this->data1,true);
		$criteria->compare('data2',$this->data2,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Config the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
