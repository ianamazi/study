//http://stackoverflow.com/a/9931925
$(document.body).delegate('select.ui-datepicker-year', 'mousedown', function() {
  (function(sel) {
    var el = $(sel);
    var ops = $(el).children().get();
    if ( ops.length > 0 && $(ops).first().val() < $(ops).last().val() ) {
      $(el).empty();
      $(el).html(ops.reverse());
    }
  })(this);
});
$(document).ready(function() {
	$('.clear-date').click(function(e){
		$txt = $(this).prev();
		$txt.val('');
		e.preventDefault();
	});
});