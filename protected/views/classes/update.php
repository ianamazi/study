<?php
/* @var $this ClassesController */
/* @var $model Classes */

$this->breadcrumbs=array(
	'Classes'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Classes', 'url'=>array('index')),
	array('label'=>'Create Class', 'url'=>array('create')),
	array('label'=>'View Class', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Classes', 'url'=>array('admin')),
);
?>

<h1>Update Class <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>