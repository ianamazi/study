<?php
/* @var $this ConfigController */
/* @var $data Config */
if (!isset($data)) {
	echo '<tr><th>Name</th><th>Type</th><th>Data1</th><th>Data2</th></tr>';
	return;
}
?>
<tr>
	<td><?php echo CHtml::link($data->name, array('view', 'id'=>$data->id)); ?></td>
	<td><?php echo isset(Config::$types[$data->type]) ? Config::$types[$data->type] : $data->type; ?></td>
	<td><?php echo CHtml::encode($data->data1); ?></td>
	<td><?php echo CHtml::encode($data->data2); ?></td>
</tr>
