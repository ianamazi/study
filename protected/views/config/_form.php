<?php
/* @var $this ConfigController */
/* @var $model Config */
/* @var $form CActiveForm */
Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl . '/js/config-edit.js')
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'config-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'action' => isset($_GET['bulk']) ? Yii::app()->createUrl('/config/create') : '', //if create bulk called, it should allow to change the type
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->dropDownList($model,'typeEx',Formatter::includeBlank(Config::$types)); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row" id="classdiv">
		<?php echo $form->labelEx($model,'class'); ?>
		<?php echo $form->dropDownList($model,'class',AppHelper::classes()); ?>
		<?php echo $form->error($model,'class'); ?>
	</div>

<?php
if ($model->isNewRecord)
{
if (isset($_GET['bulk']) || isset($_POST['chkBulk']))
	Yii::app()->getClientScript()->registerScript('show-bulk',
		'$("#chkBulk")[0].checked = true; updateBulk();');
?>
<input id="chkBulk" name="chkBulk" type="checkbox" onchange="javascript:updateBulk();" />
<label for="chkBulk" style="display: inline;">Bulk Insert</label>
<?php echo CHtml::submitButton('Get Data'); ?>
<div id="bulk-div" style="display: none;">
	<div class="row">
		<?php echo $form->labelEx($model,'bulkData'); ?>
		<?php echo CHtml::textArea('bulkData', $model->data1, array('rows'=>10, 'cols'=>62)); ?>
	</div>
</div>
<?php }
if (!isset($_POST['bulkData'])) {
?>

 <div id="non-bulk-div">
	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'data1'); ?>
		<?php echo $form->textField($model,'data1',array('size'=>60,'maxlength'=>2048)); ?>
		<?php echo $form->error($model,'data1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'data2'); ?>
		<?php echo $form->textField($model,'data2',array('size'=>60,'maxlength'=>2048)); ?>
		<?php echo $form->error($model,'data2'); ?>
	</div>
 </div>
<?php } ?>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->