<?php
/* @var $this ClassesController */
/* @var $model Classes */

$this->breadcrumbs=array(
	'Classes'=>array('index'),
	$model->subject . ' / ' . $model->name,
);

if (!UserIdentity::user_is('student'))
$this->menu=array(
	array('label'=>'List Classes', 'url'=>array('index')),
	array('label'=>'Create Class', 'url'=>array('create')),
	array('label'=>'Update Class', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Class', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Classes', 'url'=>array('admin')),
);
?>

<h1>View Class #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'year',
		array('label'=>$model->getAttributeLabel('class'), 'type'=>'raw', 'value'=>$model->class_r()),
		'subject',
		array('label'=>$model->getAttributeLabel('date_added'), 'type'=>'raw', 'value'=>Formatter::date($model->date_added)),
		array('label'=>$model->getAttributeLabel('lesson_id'), 'type'=>'raw', 'value'=>$model->lessonLink()),
		array('label'=>$model->getAttributeLabel('content'), 'type'=>'raw', 'value'=>$model->content),
	),
)); ?>
