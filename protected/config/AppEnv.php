<?php
/**
 * @author Imran Ali Namazi <imran@cselian.com>
 * @link http://tg.cselian.com/showcase/study
 * @copyright Copyright &copy; 2013-2015 cselian.com
 * @license http://tg.cselian.com/licenses#study
 * Manages the variations by domains (environments) for a Multisite Install
 */

class AppEnv
{
	private static $initialized = false;

	private static function init($dom)
	{
		self::$initialized = true;
		
		//$data[$dom]['AcademicYearStart'] = 2015; // TODO: let users set this somehow

		if ($dom == 'localhost') self::$data[$dom]['gii'] = 1;

		if ($dom == 'localhost') return;

		$cfg = dirname(__FILE__) . '/nongit.php';
		$db = file_exists($cfg) ? include_once($cfg) : array();
		foreach ($db as $key=>$val)
			self::$data[$dom]['db'][$key] = $val;
	}

	private static $data = array(
		'localhost' => array(
			'name' => 'CStudy :: Smart Learning'
			, 'code' => 'dev'
			, 'testEmail' => 'imran@cselian.com'
			, 'db' => array('connectionString' => 'mysql:host=localhost;dbname=study', 'username' => 'root', 'password' => '')
			, 'nonProduction'=> 1
		),
		'study.cselian.com' => array(
			'name' => 'CStudy :: Smart Learning'
			, 'code' => 'live'
			, 'db' => array('connectionString' => 'mysql:host=localhost;dbname=cselian_study', 'username' => 'nongit', 'password' => 'nongit')
		),
	);

	// Helper function for Array
	public static function valOrDefault($array, $name, $default)
	{
		return isset($array[$name]) ? $array[$name] : $default;
	}

	public static function getArrayVal($var, $key, $name)
	{
		$arr = self::get($var, $name);
		return $arr ? $arr[$key] : false;
	}

	public static function get($var, $name, $default = null)
	{
		return self::getDomainVar($var, $name, $default);
	}

	public static function db()
	{
		return self::getDomainVar('db', 'Connection String');
	}

	public static function name()
	{
		return self::getDomainVar('name', 'Site Name', 'CStudy :: Classes for Students');
	}

	private static function getDomainVar($var, $friendly, $default = null)
	{
		$dom = $_SERVER['HTTP_HOST'];

		if (!isset(self::$data[$dom]))
		{
			if ($default !== null) return $default;
			throw new Exception(sprintf('Domain "%s" not configured!', $dom));
		}
		
		if (!self::$initialized) self::init($dom);
		$domData = self::$data[$dom];
		if (!isset($domData[$var]))
		{
			if ($default !== null) return $default;
			throw new Exception(sprintf('%s not configured for domain "%s"!', $friendly, $dom));
		}

		return $domData[$var];
	}

	private static function contains($haystack, $needle) { return gettype(strpos($haystack, $needle)) == "integer"; }
}
