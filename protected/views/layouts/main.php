<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/site.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<?php if (UserIdentity::user_is('notloggedin')) { ?><span style="float: right; margin: 10px;">user: scribe@cselian.com / pass: study18</span><?php } ?>
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainMbMenu" class="nav popout">
		<?php $this->widget('application.extensions.mbmenu.MbMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>Yii::app()->baseUrl, 'items' => array(
					array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
					array('label'=>'Contact', 'url'=>array('/site/contact')),
				)),
				array('label'=>'Administer', 'visible'=>UserIdentity::user_is('admin'), 'items' => array(
					array('label'=>'Users', 'url'=>array('/users/admin'), 'items'=> array(
						array('label'=>'Create', 'url'=>array('/users/create'))
					)),
					array('label'=>'Config', 'url'=>array('/config/index'), 'items' => array(
						array('label'=>'Class', 'url'=>array('/config/create?bulk=1&type=class')),
						array('label'=>'Subject', 'url'=>array('/config/create?bulk=1&type=subject')),
						array('label'=>'Teacher', 'url'=>array('/config/create?bulk=1&type=teacher')),
					)),
					array('label'=>'Generator', 'url'=>array('/gii'), 'visible' => UserIdentity::user_is('dev') && AppEnv::get('gii', 'Enable Code Generator', 0), 'linkOptions' => array('target' => 'gii')),
					)),
				array('label'=>'Teacher', 'visible'=>UserIdentity::user_is('teacher'), 'items' => array(
					array('label'=>'Lessons', 'url'=>array('/lessons/admin'), 'items'=> array(
						array('label'=>'Create', 'url'=>array('/lessons/create'))
					)),
					array('label'=>'Classes', 'url'=>array('/classes/admin'), 'items'=> array(
						array('label'=>'Create', 'url'=>array('/classes/create'))
					)),
				)),
				array('label'=>'Student', 'visible'=>UserIdentity::user_is('student'), 'items' => array(
					array('label'=>'Lessons', 'url'=>array('/lessons/')),
					array('label'=>'Classes', 'url'=>array('/classes/')),
				)),
				AppHelper::subject_links(),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>UserIdentity::user_is('notloggedin')),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>UserIdentity::user_is('loggedin'))
			),
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		<span style="float: right;">Powered by <a href="http://www.yiiframework.com/" target="_blank">Yii Framework</a></span>
		Copyright &copy; 2015 - <?php echo date('Y'); ?> by Cselian Technology Group and Imran Ali Namazi.<br />
		Some lessons may be copyright their boards / schools.
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
