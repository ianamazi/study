function updateBulk()
{
	$bulk = $('#chkBulk')[0].checked;
	if ($bulk)
	{
		$('#bulk-div').show();
		$('#non-bulk-div').hide();
	}
	else
	{
		$('#non-bulk-div').show();
		$('#bulk-div').hide();
	}
}

$(document).ready(function() {
	if ($('#Config_typeEx').val() != 'classtimetable')
		$('#classdiv').hide();
	$('#Config_typeEx').change(function(e) {
		if ($('#Config_typeEx').val() != 'classtimetable')
			$('#classdiv').hide();
		else
			$('#classdiv').show();
	});
});
