<?php $this->pageTitle=Yii::app()->name; ?>

<h1>Welcome</h1>

<p>CStudy enables a school to manage its classes online. Students can have tablets in the classroom and connect to this website</p>

<p><?php TimeTable::render(); ?></p>

<p>This site is still in 
<a href="http://en.wikipedia.org/wiki/Software_release_life_cycle#Beta" target="_blank">beta</a>,
so you are encouraged to see the 
<a href="https://bitbucket.org/ianamazi/study/wiki/Home" target="_blank">wiki</a> or log 
<a href="https://bitbucket.org/ianamazi/study/issues" target="_blank">issues</a>.
Please make sure to look for similar issues before creating new ones</p>
