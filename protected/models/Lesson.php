<?php

/**
 * This is the model class for table "lessons".
 *
 * The followings are the available columns in table 'lessons':
 * @property integer $id
 * @property string $name
 * @property integer $class
 * @property string $subject
 * @property integer $chapter
 * @property string $content
 */
class Lesson extends CActiveRecord
{
	public function class_r()
	{
		$val = $this->class;
		if (intval($val) == 0) return $val;
		$suffixes = array(1 => 'st', 2 => 'nd', 3 => 'rd');
		$suffix = isset($suffixes[$val]) ? $suffixes[$val] : 'th';
		return $val . $suffix;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lessons';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, class, subject, chapter, content', 'required'),
			array('class, chapter', 'numerical', 'integerOnly'=>true),
			array('name, subject', 'length', 'max'=>100),
			array('content', 'length', 'max'=>8192),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, class, subject, chapter, content', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'class' => 'Class / Department',
			'subject' => 'Subject',
			'chapter' => 'Chapter',
			'content' => 'Content',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('class',$this->class);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('chapter',$this->chapter);
		$criteria->compare('content',$this->content,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Lesson the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
