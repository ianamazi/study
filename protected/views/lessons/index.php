<?php
/* @var $this LessonsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Lessons',
);

if (!UserIdentity::user_is('student'))
$this->menu=array(
	array('label'=>'Create Lesson', 'url'=>array('create')),
	array('label'=>'Manage Lessons', 'url'=>array('admin')),
);
?>

<h1>Lessons</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
