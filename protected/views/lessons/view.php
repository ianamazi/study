<?php
/* @var $this LessonsController */
/* @var $model Lesson */

$this->breadcrumbs=array(
	'Lessons'=>array('index'),
	$model->name,
);

if (!UserIdentity::user_is('student'))
$this->menu=array(
	array('label'=>'List Lessons', 'url'=>array('index')),
	array('label'=>'Create Lesson', 'url'=>array('create')),
	array('label'=>'Update Lesson', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Lesson', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Lessons', 'url'=>array('admin')),
);
?>

<h1>View Lesson #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		array('label'=>$model->getAttributeLabel('class'), 'type'=>'raw', 'value'=>$model->class_r()),
		'subject',
		'chapter',
		array('label'=>$model->getAttributeLabel('content'), 'type'=>'raw', 'value'=>$model->content)
	),
)); ?>
