<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$gitExcludedFile = YiiBase::getPathOfAlias('webroot') . '/protected/components/nongit.php';
		$admin=array(
			'imran'=> file_exists($gitExcludedFile) ? include_once($gitExcludedFile) : 'study786',
		);

		if(isset($admin[$this->username]))
		{
			$ok = $admin[$this->username]==$this->password;
			if ($ok) Yii::app()->session['user'] = array('type' => User::$typeAdmin, 'class' => '0');
			$this->errorCode= $ok ? self::ERROR_NONE : self::ERROR_PASSWORD_INVALID;
		}
		else
		{
			$usr = new User; $usr = $usr->find('email = "'.$this->username.'"');
			if ($usr)
			{
				$ok = $usr->password == md5($this->password);
				if ($ok) Yii::app()->session['user'] = $usr->attributes;
				$this->errorCode = $ok ? self::ERROR_NONE : self::ERROR_PASSWORD_INVALID;
			}
			else
			{
				$this->errorCode = self::ERROR_USERNAME_INVALID;
			}
		}
		return !$this->errorCode;
	}

	static function user_is($what)
	{
		$user = Yii::app()->user;
		//use session in case machine has been restarted
		if ($what == 'notloggedin') return $user->isGuest || !isset(Yii::app()->session['user']);
		if ($what == 'loggedin') return !$user->isGuest && isset(Yii::app()->session['user']);
		if ($what == 'dev') return $user->name == 'imran';

		if ($user->isGuest) return false;

		$type = Yii::app()->session['user']['type'];
		if ($what == 'student' && $type == User::$typeStudent)
			return true;
		if ($what == 'teacher' && ($type == User::$typeTeacher || $type == User::$typeAdmin))
			return true;
		if ($what == 'admin' && $type == User::$typeAdmin)
			return true;

		return false;
	}
}