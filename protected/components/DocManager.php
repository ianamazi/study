<?php
class DocManager
{
	public static function addWidget($c)
	{
		$controller = $c->getId();
		if ($controller != 'lessons' && $controller != 'classes') return;

		$action = $c->getAction()->getId();
		if ($action != 'update' && $action != 'create' && $action != 'view') return;

		$id = Yii::app()->request->getParam('id');

		$help = $action == 'update' ? ' - <span title="Upload files, then right click and copy the link to use on the left (link/image)">?</span>' : '';
		$c->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Images / Files' . $help,
		));
		$fol = Yii::app()->basePath . '/../docs/' . $controller . '/' . $id;
		//die($fol);
		if ($action == 'view' && !is_dir($fol))
			echo 'No Files';
		else if ($action == 'create')
			echo 'Save this page first before you can upload files.';
		else
			echo sprintf('<iframe src="%s/docs/?fol=%s/%s%s" height="600"></iframe>',
			Yii::app()->baseUrl, $controller, $id, $action == 'view' ? '&view=1' : '');
		$c->endWidget();
	}
}
?>
