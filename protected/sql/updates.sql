ALTER TABLE `classes` ADD COLUMN `teacher` varchar(100) NOT NULL after subject;

ALTER TABLE `users` MODIFY COLUMN `deactive` tinyint(1) NOT NULL;

