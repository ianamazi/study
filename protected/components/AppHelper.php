<?php
class AppHelper
{
	public static function classes($nosections = false)
	{
		$classes = Config::byType('class');
		$op = array('' => 'Not Set');
		foreach ($classes as $cls)
			$op[$cls->name] = $cls->name;
		return $op;
	}

	public static function subjects($class = 0, $empty = false)
	{
		//NB: no sorting applied. its in the order found in the file
		$subjects = Config::byType('subject');
		$op = $empty ? array() : array('' => 'Not Set');
		foreach ($subjects as $s)
		{
			$pass = false;
			if ($class != 0 && $s->data1 != '*')
			{
				$end = substr($s->data1, -1);
				$val = intval(str_replace('+', '', str_replace('-', '', $s->data1)));
				if ($end === '+') {
					$pass = $class < $val;
				} else if ($end === '-') {
					$pass = $class > $val;
				} else {
					$ok = explode(',', $s->data1);
					if (array_search($class . '', $ok) === false)
						$pass = true;
				}
			}
			if ($pass) continue;
			if (stripos($s->name, '=') !== false)
			{
				$kv = explode('=', $s->name);
				$s->name = $kv[0];
				$sub = array();
				foreach (explode('|', $kv[1]) as $ss) $sub[$ss] = $ss;
				$op[$s->name] = $sub;
			}
			else
			{
				$op[$s->name] = $s->name;
			}
		}
		ksort($op);
		return $op;
	}

	public static function subject_links()
	{
		if (UserIdentity::user_is('notloggedin')) return null;

		$usr = Yii::app()->session['user'];
		//$usr = array('type' => User::$typeTeacher, 'class' => '0');
		//$usr = array('type' => User::$typeStudent, 'class' => '8');

		$cls = explode(',', $usr['class']);
		$cls = intval($cls[0]); //TODO: using User:class_val throws property on non-object error
		$subjects = self::subjects($cls, true);

		$result = array(
			'label'=>'Subjects' . ($usr['type'] != User::$typeStudent ? ' (All)' : ' (Class ' . $cls . ')'),
			'url'=>array(UserIdentity::user_is('student') ? '/classes' : '/lessons'),
			'visible'=>UserIdentity::user_is('loggedin')
		);

		if (!count($subjects)) return $result;

		$c = $usr['type'] == User::$typeStudent ? '/classes/?subject=' : '/lessons/?subject=';
		$op = array();
		foreach ($subjects as $s)
			if (is_array($s))
				foreach ($s as $sub) $op[] = array('label'=>$sub, 'url'=>array($c. $sub));
			else
				$op[] = array('label'=>$s, 'url'=>array($c. $s));
		$result['items'] = $op;

		return $result;
	}
}
?>